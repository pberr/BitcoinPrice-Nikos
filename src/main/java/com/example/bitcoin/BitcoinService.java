package com.example.bitcoin;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

@Service
public class BitcoinService {

    public String getBitcoinPrice() {
        try{
            URL url = new URL("https://api.coindesk.com/v1/bpi/currentprice.json");
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();
            if(responsecode != 200)
                System.err.println(responsecode);
            else{
                Scanner sc = new Scanner(url.openStream());
                String inline="";
                while(sc.hasNext()){
                    inline+=sc.nextLine();
                }
                sc.close();

                JSONParser parse = new JSONParser();
                JSONObject jObj = (JSONObject)parse.parse(inline);
                JSONObject jObj1= (JSONObject) jObj.get("bpi");
                JSONObject jObj_2 = (JSONObject) jObj1.get("EUR");

                String price =jObj_2.get("rate_float").toString();
                return price;
            }
		} catch (IOException | org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		return null;
    }

}